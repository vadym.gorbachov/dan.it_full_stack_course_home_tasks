"use strict"

const VISIBLE_ITEM = 'visible-item';
const serviceMenu = document.querySelector('.our-services-menu');
const serviceTabItems = document.querySelectorAll('.our-services-menu-link');
const servicePictures = document.querySelectorAll('.service-picture');
const serviceTexts = document.querySelectorAll('.our-services-text');
const portfolioMenu = document.querySelector('.portfolio-menu');
const portfolioTabItems = document.querySelectorAll('.portfolio-menu-link');
let portfolioImageContainers = document.querySelectorAll('.portfolio-image-item');
const portfolioImageList = document.querySelector('.portfolio-images-list');
const portfolioUploadButton = document.querySelector('.portfolio-button');
let portfolioImageIndex = 0;
const imageList = [
  {path: '13', category: 'graphic-design'},
  {path: '14', category: 'graphic-design'},
  {path: '15', category: 'graphic-design'},
  {path: '16', category: 'graphic-design'},
  {path: '17', category: 'graphic-design'},
  {path: '18', category: 'web-design'},
  {path: '19', category: 'web-design'},
  {path: '20', category: 'web-design'},
  {path: '21', category: 'web-design'},
  {path: '22', category: 'web-design'},
  {path: '23', category: 'word-press'},
  {path: '24', category: 'word-press'},
  {path: '25', category: 'word-press'},
  {path: '26', category: 'word-press'},
  {path: '27', category: 'word-press'},
  {path: '28', category: 'word-press'},
  {path: '29', category: 'landing-pages'},
  {path: '30', category: 'landing-pages'},
  {path: '31', category: 'landing-pages'},
  {path: '32', category: 'landing-pages'},
  {path: '33', category: 'landing-pages'},
  {path: '34', category: 'landing-pages'},
  {path: '35', category: 'landing-pages'},
  {path: '36', category: 'landing-pages'},
];

const previousReviewerButton = document.querySelector('.previous-reviewer');
const nextReviewerButton = document.querySelector('.next-reviewer');
const reviewersList = document.querySelector('.reviewers-list');
const reviewers = document.querySelectorAll('.reviewer-list-item');
const reviewerBigImage = document.querySelector('.big-image');
const reviewerName = document.querySelector('.reviewer');
const reviewerProfession = document.querySelector('.reviewer-profession');
const newsImagesContainer = document.querySelector('.news-images-list');
const newsImagesList = document.querySelectorAll('.breaking-news-image');

const allReviewers = [
  {path: '1', name: 'Hasan Ali', profession: 'UX Designer'},
  {path: '2', name: 'Topaz Smith', profession: 'QA'},
  {path: '3', name: 'Samuel Williams', profession: 'Developer'},
  {path: '4', name: 'Frank Brown', profession: 'Project Manager'},
  {path: '5', name: 'Raymond Jones', profession: 'Developer'},
  {path: '6', name: 'Patrick Miller', profession: 'UX Designer'},
  {path: '7', name: 'Henry Moore', profession: 'QA'},
  {path: '8', name: 'Roger Thompson', profession: 'DBA'},
  {path: '9', name: 'Vadym Horbachov', profession: 'Developer'}
];
let index = 0;
const iconsCount =  reviewersList.children.length;

previousReviewerButton.addEventListener('click', showPreviousReviewers);
nextReviewerButton.addEventListener('click', showNestReviewers);
reviewersList.addEventListener('click', upImage);
serviceMenu.addEventListener('click', switchService);
newsImagesContainer.addEventListener('click', upActivePicture);
portfolioUploadButton.addEventListener('click', getMorePortfolioImages);
portfolioMenu.addEventListener('click', switchPortfolio);

reviewerName.innerHTML = allReviewers[1].name;
reviewerProfession.innerHTML = allReviewers[1].profession;
reviewerBigImage.src = reviewerBigImage.src.replace(/\d+\.jpg/, (2) + '.jpg');


function switchService(e){
   e.preventDefault();
    for (let i = 0; i < serviceTabItems.length; i++){
      serviceTabItems[i].classList.remove('active-service-menu-link');
    }
    e.target.classList.add('active-service-menu-link');
    let className = e.target.className.split(' ')[0];
    switchServiceText(className);
    switchServiceImages(className);
}

function switchServiceText(className){
  for (let i = 0; i < serviceTexts.length; i++){
    if (className === serviceTexts[i].className.split(' ')[0]){
      serviceTexts[i].classList.add(VISIBLE_ITEM);
    }else {
      serviceTexts[i].classList.remove(VISIBLE_ITEM);
    }
  }
}

function switchServiceImages(className){
 for (let i = 0; i < servicePictures.length; i++){
   if (className === servicePictures[i].className.split(' ')[0]){
     servicePictures[i].classList.add(VISIBLE_ITEM);
   }else {
     servicePictures[i].classList.remove(VISIBLE_ITEM);
   }
  }
}

function switchPortfolio(e){
  e.preventDefault();
  if (e.target.classList.contains('all-images')){
    showAllPortfolioImages();
    switchActiveTab(e);
    return;
  }
  switchActiveTab(e);
  switchPortfolioImages(e.target.className.split(' ')[0]);
}

function switchActiveTab(e){
  for (let i = 0; i < portfolioTabItems.length; i++){
    portfolioTabItems[i].classList.remove('portfolio-menu-active-link');
    e.target.classList.add('portfolio-menu-active-link');
  }
}

function showAllPortfolioImages() {
  for (let i = 0; i < portfolioImageContainers.length; i++) {
    if (!portfolioImageContainers[i].classList.contains('portfolio-visible-item')) {
      portfolioImageContainers[i].classList.add('portfolio-visible-item');
    }
  }
}

function switchPortfolioImages(className) {
  portfolioImageContainers = document.querySelectorAll('.portfolio-image-item');
  for (let i = 0; i < portfolioImageContainers.length; i++) {
    if (className === portfolioImageContainers[i].className.split(' ')[0]) {
      if (!portfolioImageContainers[i].classList.contains('portfolio-visible-item')) {
        portfolioImageContainers[i].classList.add('portfolio-visible-item');
      }
      } else {
        portfolioImageContainers[i].classList.remove('portfolio-visible-item');
      }
  }
}

function getMorePortfolioImages(e){
  e.preventDefault();
  const count = portfolioImageIndex;
for (let i = 0; i < 12; i++){
  portfolioImageIndex++;
  const newImageContainer = portfolioImageContainers[0].cloneNode(true);
  newImageContainer.childNodes[3].src =
    newImageContainer.childNodes[3].src.replace(
      'portfolio_1', 'all/portfolio_' + imageList[i + count].path
    );
  newImageContainer.className = imageList[i + count].category
    + ' portfolio-image-item portfolio-visible-item';
  portfolioImageList.append(newImageContainer);
  if (portfolioImageIndex >= 24){
    portfolioUploadButton.style.display = 'none';
  }
}
}

function showPreviousReviewers(){
  resetIndexLeft();
  changeActiveInLeft();
  rebuildStreamLeft();
}

function showNestReviewers(){
  ressetIndexRight();
  changeActiveInRight();
  rebuildStreamRight();
}

function upImage(e){
if (!e.target.parentNode.classList.contains('reviewer-menu')
&& !e.target.parentNode.classList.contains('reviewers-list')){
  for (const reviewer of reviewersList.children) {
    reviewer.classList.remove('active');
  }
  e.target.parentNode.classList.add('active');
  switchReviewerBigImageByActiveIcon();
}
}

function rebuildStreamRight(){
  let newReviewer = reviewers[0].cloneNode(true);
  newReviewer.childNodes[1].src = newReviewer.childNodes[1].src.replace(
    /\d+\.jpg/, (++index + iconsCount) + '.jpg');
  reviewersList.append(newReviewer);
  reviewersList.children[0].remove();
  switchReviewerBigImageByActiveIcon('active');
}


function rebuildStreamLeft(){
  let newReviewer = reviewers[0].cloneNode(true);
  newReviewer.childNodes[1].src = newReviewer.childNodes[1].src.replace(
    /\d+\.jpg/, (index - iconsCount) + '.jpg');
  --index;
  reviewersList.prepend(newReviewer);
  reviewersList.children[iconsCount].remove();
  switchReviewerBigImageByActiveIcon('active');
}

function changeActiveInRight(){
  for (let i = reviewersList.children.length - 1; i >= 0; i--) {
    if (reviewersList.children[i].classList.contains('active')){
      reviewersList.children[i].classList.remove('active');
      reviewersList.children[i + 1].classList.add('active');
      reviewerBigImage.src = reviewersList.children[i + 1].children[0].src
    }
  }
}

function changeActiveInLeft(){
  for(let i = 1; i < reviewersList.children.length - 1; i++){
    if (reviewersList.children[i].classList.contains('active')){
      reviewersList.children[i].classList.remove('active');
      reviewersList.children[i - 1].classList.add('active');
      reviewerBigImage.src = reviewersList.children[i + 1].children[0].src;
    }
  }
}

function ressetIndexRight(){
  if (index >= allReviewers.length - iconsCount){
    index = - iconsCount;
  }
}

function resetIndexLeft(){
  if (index <= 6){
    index = allReviewers.length + iconsCount;
  }
}

function upActivePicture(e){
  e.preventDefault();
  if (e.target.classList.contains('breaking-news-image')){
    for (let i = 0; i <  newsImagesList.length; i++){
      if (newsImagesList[i].classList.contains('portfolio-image-item-active')){
        newsImagesList[i].classList.remove('portfolio-image-item-active');
      }
    }
    e.target.classList.add('portfolio-image-item-active');
  }
  switchReviewerBigImageByActiveIcon();
}

function switchReviewerBigImageByActiveIcon(){
  const activeIcon = document.querySelector('.active');
      reviewerBigImage.src = activeIcon.children[0].src;
      let reviewerIndex = activeIcon.children[0].src.match(/[\d+]/)[0];
      --reviewerIndex;
      reviewerName.innerHTML = allReviewers[reviewerIndex].name;
      reviewerProfession.innerHTML = allReviewers[reviewerIndex].profession;
}