const { watch, series } = require("gulp");
const { jsProcessor } = require('./js.processor.js');
const { browserSync } = require('./browser.sync.js');
const { sassProcessor } = require('./scss.processor.js');

function watcher(){
  watch('*.html').on('change', browserSync.reload);
  watch('./src/js/*.js').on('change', series(jsProcessor, browserSync.reload));
  watch('./src/scss/*.scss').on('change', series(sassProcessor, browserSync.reload));
}

exports.watcher = watcher;