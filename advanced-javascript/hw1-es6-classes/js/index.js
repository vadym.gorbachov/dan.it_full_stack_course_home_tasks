class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

   setName(name){
        this._name = name;
   }

   getName(){
        return this._name;
   }

   setAge(age){
        this._age = age;
   }

   getAge(){
        return this._age;
   }

   setSalary(salary){
        this._salary = salary;
   }

   getSalary(){
        return this._salary;
   }

   toString(){
        console.log(`name: ${this.getName()} age: ${this.getAge()} salary: ${this.getSalary()}`)
   }
}

class Programmer extends Employee{
constructor(name, age, salary, ...lang) {
    super(name, age, salary);
    this._lang = lang;
}
    getSalary() {
        return this._salary * 3;
    }

    toString() {
        console.log(`name: ${this.getName()} age: ${this.getAge()} salary: ${this.getSalary()} lang: ${this._lang}`)
        ;
    }
}

const programmer_one = new Programmer('Mike', 23, 800, 'spanish', 'german');
const programmer_two = new Programmer('Aleksandr', 27, 1600, 'russian', 'english');
const programmer_three = new Programmer('Karl', 32, 3500, 'italian', 'english');

console.log(programmer_one.toString());
console.log(programmer_two.toString());
console.log(programmer_three.toString());

