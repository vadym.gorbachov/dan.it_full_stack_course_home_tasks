'use strict';

class Post{
  constructor(id, userId, title, body) {
    this.id = id;
    this.userId = userId;
    this.title = title;
    this.body = body;
  }
}

class Twiter{
  users = [];
  posts = [];
  postUserSurname;

  constructor() {
    this.USERS_URL = 'https://ajax.test-danit.com/api/json/users';
    this.POSTS_URL = 'https://ajax.test-danit.com/api/json/posts';
    this.CREATE_POST_URL = 'https://ajax.test-danit.com/api/json/posts';
    this.DELETE_POST_URL = 'https://ajax.test-danit.com/api/json/posts/';
    this.PUT_POST_URL = 'https://ajax.test-danit.com/api/json/posts/';
  }

  async doHttpRequest(url, method, data){
    const response = await axios({
      method: method,
      url: url,
      body: data
    })
    return response;
  }

async initialise(){
  await this.getCurrentContent();
  this.initialiseHtmlContent();
  this.contentRender()
}

initialiseHtmlContent(){
  this.root = document.getElementsByClassName('root')[0];
  this.form = document.getElementById("post-form").content.children[0];
  this.form.classList.add('post-form');
  this.form.querySelector('.form-submit').addEventListener('click', (e) => {
    e.preventDefault();
    this.createPost(e);
  })
  this.postList = document.createElement('ul');
  this.postContainer = document.createElement('li');
  this.postContainer.classList.add('post__card');
  this.postTitle = document.createElement('h3');
  this.postTitle.classList.add('post__title');
  this.postText = document.createElement('p');
  this.postText.classList.add('post__text');
  this.postUserName = document.createElement('p');
  this.postUserName.classList.add('post__user-name');
  this.postUserSurname = document.createElement('post__user-surname');
  this.postUserEmail = document.createElement('p');
  this.postUserEmail.classList.add('post__user-email');
  this.addButton = document.createElement('span');
  this.addButton.classList.add('post__add-button');
  this.editButton = document.createElement('span');
  this.editButton.classList.add('post__edit-button');
  this.deleteButton = document.createElement('span');
  this.deleteButton.classList.add('post__delete-button');
}

contentRender(){
    for(const post of this.posts){
      const user = this.users.find((user) => {
        if (user.id == post.userId) return true;
      })
      const newPostContainer = this.postContainer.cloneNode('true');
      newPostContainer.classList.add(`id_${post.id}`);
      const newPostTitle = this.postTitle.cloneNode('true');
      newPostTitle.textContent = post.title;
      newPostContainer.appendChild(newPostTitle);
      const newPostText = this.postText.cloneNode('true');
      newPostText.textContent = post.body;
      newPostContainer.appendChild(newPostText);
      const newPostUserName = this.postUserName.cloneNode('true');
      newPostUserName.textContent = user.name.split(' ')[0];
      newPostContainer.appendChild(newPostUserName);
      const newPostUserSurname = this.postUserSurname.cloneNode('true');
      newPostUserSurname.textContent = user.name.split(' ')[1];
      newPostContainer.appendChild(newPostUserSurname);
      const newPostUserEmail = this.postUserEmail.cloneNode('true');
      newPostUserEmail.textContent = user.email;
      newPostContainer.appendChild(newPostUserEmail);
      const newAddButton = this.addButton.cloneNode('true');
      newAddButton.addEventListener('click', (e) =>{
        this.addPost(e);
      });
      newPostContainer.appendChild(newAddButton);
      const newEditButton = this.editButton.cloneNode('true');
      newEditButton.addEventListener('click', (e) =>{
        this.editPost(e);
      });
      newPostContainer.appendChild(newEditButton);
      const newDeleteButton = this.deleteButton.cloneNode('true');
      newDeleteButton.addEventListener('click', (e) =>{
        this.deletePost(e);
      });
      newPostContainer.appendChild(newDeleteButton);
      this.postList.appendChild(newPostContainer);
    }
    this.root.appendChild(this.postList);
}

async getCurrentContent(){
  const usersPromise = await this.doHttpRequest(this.USERS_URL, 'GET');
  const postsPromise = await this.doHttpRequest(this.POSTS_URL, 'GET');
  this.users = usersPromise.data;
  this.posts = postsPromise.data;
  console.log(this.posts[0])
}

addPost(e){
    this.root.prepend(this.form);
    this.form.style.display = 'block';
  }

editPost(e){
    console.log('editButton')
    console.log(e.target.parentNode.classList[1].split('_')[1])
  }

async  createPost(e){
    e.target.parentNode.style.display = 'none';
    const form = e.target.parentNode;
    try{
      const response = await this.doHttpRequest(
        this.CREATE_POST_URL,
        'post',
        {
          body: form.querySelector('#text').value,
          title: form.querySelector('#title').value,
          userId: 1,
        })
    }catch (e){
      console.log(e.message)
    }
// Imitation creating post by server response with new id
const post = new Post(
  42, 1,
  form.querySelector('#title').value,
  form.querySelector('#text').value
)

  }

async deletePost(e){
  const id = e.target.parentNode.classList[1].split('_')[1];
  const url = this.DELETE_POST_URL + id;
  const response = await this.doHttpRequest(url, 'DELETE');
  if (response.status == 200){
    e.target.parentNode.parentNode.removeChild(e.target.parentNode);
}
  }

}
const twiter = new Twiter();
twiter.initialise();








