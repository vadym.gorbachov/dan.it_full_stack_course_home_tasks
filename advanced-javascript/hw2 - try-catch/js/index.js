const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

class InvalidBookError{
    constructor(message) {
        this.message = message;
    }
    name = 'InvalidBookError';
}

function bookIsValid(book){
    if (!book.hasOwnProperty('author')){
        throw new InvalidBookError(`Book ${book['author']}, ${book['name']}, ${book['price']} das not have the property: author`);
    }
    if (!book.hasOwnProperty('name')){
        throw new InvalidBookError(`Book ${book['author']}, ${book['name']}, ${book['price']} das not have the property: name`);
    }
    if (!book.hasOwnProperty('price')){
        throw new InvalidBookError(`Book ${book['author']}, ${book['name']}, ${book['price']} das not have the property: price`);
    }
}

function renderBooks(books){
    const root = document.getElementById('root');
    const ulBooks = document.createElement('ul');
    ulBooks.classList.add('books__list');
    const ulBook = document.createElement('li');
    const ulBookProperty = document.createElement('ul');
    ulBookProperty.classList.add('books__item-property');
    ulBook.classList.add('books__item');

    for (let book of books) {
        try{
            bookIsValid(book);
            let newUlBook = buildBook(book, ulBook, ulBookProperty);
            ulBooks.appendChild(newUlBook);
        }catch (e){
            if (e.name == 'InvalidBookError'){
                console.log(e.message);
            }else throw new Error(e.message);
        }
    }
    root.appendChild(ulBooks);
}

function buildBook(book, ulBook, ulBookProperty){
    let newUlBook = ulBook.cloneNode(true);

    let newUlBookProperty = ulBookProperty.cloneNode(true);
    newUlBookProperty.textContent = book.author;
    newUlBook.appendChild(newUlBookProperty);

    newUlBookProperty = ulBookProperty.cloneNode(true);
    newUlBookProperty.textContent  = book.name;
    newUlBook.appendChild(newUlBookProperty);

    newUlBookProperty = ulBookProperty.cloneNode(true);
    newUlBookProperty.textContent  = book.price;
    newUlBook.appendChild(newUlBookProperty);
    return newUlBook;
}

renderBooks(books);
