console.log('TASK3');

const user1 = {
    name: "John",
    years: 30
};

let {name, years : age, isAdmin = 'false'} = user1;

console.dir({name, age, isAdmin});
