console.log('TASK7');

const array = ['value', () => 'showValue'];

// Допишите ваш код здесь
const {value = 'value', showValue = function(){
    return 'showValue';
}} = array;

alert(value); // должно быть выведено 'value'
alert(showValue());  // должно быть выведено 'showValue'
