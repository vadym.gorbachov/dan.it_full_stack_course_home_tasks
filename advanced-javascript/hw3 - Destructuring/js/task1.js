console.log('TASK1');

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const unionClients = [...clients1, ...clients2];

const unionClientsSet = new Set(unionClients);

console.dir(unionClientsSet);
