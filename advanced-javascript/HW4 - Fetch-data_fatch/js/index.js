"use strict"

const FILMS_REQUEST = 'https://ajax.test-danit.com/api/swapi/films';

class Films{
    constructor(root = document.getElementById('root')) {
      this.root = root;
  }

  render(){
      this.getDataByFatch(FILMS_REQUEST)
        .then((response) => {
            this.films = response;
            this.renderFilms();
        })
      .then(()=>{
        this.renderCharacters();
      })
  }

  renderCharacters(){
    const ul = document.createElement('ul');
    let ulCharacters = document.createElement('ul');
      for(const film of this.films){
        let li = document.getElementsByClassName(`episode_${film['episodeId']}`)[0];
        for(const characterUrl of film['characters']){
          this.dataPromise = this.getDataByFatch(characterUrl);
          this.dataPromise
            .then((response) => {
              ulCharacters = ul.cloneNode(true);
              ulCharacters.classList.add('film__characters-list');
              ulCharacters.innerHTML = 'Characters:';
              const liCharacter = document.createElement('li');
              liCharacter.classList.add('character');
              liCharacter.innerHTML = response.name;
              ulCharacters.appendChild(liCharacter);
               li.appendChild(ulCharacters);
            })
        }
      }
  }

  async getDataByFatch(url){
    return (await fetch(url, {
      method: "GET",
      headers: {
            "Content-Type": "application/json",
        },
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'omit',
      redirect: 'error',
    })).json();
  }

  renderFilms(){
    this.ulFilms = document.createElement('ul');
    this.ulFilms.classList.add('film_list');
    this.ulFilmName = document.createElement('li');
    this.ulFilmName.classList.add('films__item-name');
    this.ulEposode = document.createElement('ul');
    this.ulEposode.classList.add('films__item-episode');
    this.ulTitle = document.createElement('ul');
    this.ulTitle.classList.add('films__item-title');
    this.ulOpeningCrawl = document.createElement('ul');
    this.ulOpeningCrawl.classList.add('films__item-opening-crawl');
    this.buildFilmList();
  }

  buildFilmList(){
    for(const film of this.films){
      const ulFilmName = this.ulFilmName.cloneNode(true);
      ulFilmName.classList.add(`episode_${film['episodeId']}`);
      ulFilmName.textContent = film.name;
          const ulFilmEposode = this.ulEposode.cloneNode(true);
          ulFilmEposode.textContent = `Episode: ${film.episodeId}`;
          ulFilmName.appendChild(ulFilmEposode);
          const ulOpeningCrawl = this.ulEposode.cloneNode(true);
          ulOpeningCrawl.textContent = film.openingCrawl;
          ulFilmName.appendChild(ulOpeningCrawl);
      this.ulFilms.appendChild(ulFilmName);
    }
    this.root.appendChild(this.ulFilms);
  }
}

const films = new Films();
films.render();
