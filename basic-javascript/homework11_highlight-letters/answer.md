## Теоретический вопрос

1. Почему для работы с input не рекомендуется использовать события клавиатуры?

## Задание

2. Потому что события клавиатуры не составляют все множество событий, которые могут произойти для тега "input". Как следвстие вышесказаного, мы не сможем таким спомобом обработать все события, обрабатывая только события клавиатуры.