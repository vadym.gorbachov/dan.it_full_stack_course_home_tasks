"use strict";

const buttons = document.getElementsByClassName("btn");

function onClickButton(event){
  for (const button of buttons) {
    button.dataset.key
    if (
       (event.code === "Key" + button.dataset.key
      && !event.shiftKey)
        || (event.code === button.dataset.key
    && !event.shiftKey)
    ) {
      button.classList.add("active");
    } else {
		if (!event.shiftKey){
      button.classList.remove("active");
		}
    }
  }
}

document.addEventListener('keydown', onClickButton);
