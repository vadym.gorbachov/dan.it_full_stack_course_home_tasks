"use strict";

const FIRST_NUMBER_MESSEGE = 'This resourse can show you all simple number in a range. Please enter the start number';
const SECOND_NUMBER_MESSAGE = 'This resourse can show you all simple number in a range. Please enter the second number';
let validationUserNumbers = false;
let response = '';
let firstNumber = '';
let secondNumber = '';

do{
    firstNumber = prompt(FIRST_NUMBER_MESSEGE);
    secondNumber = prompt(SECOND_NUMBER_MESSAGE);
    validationUserNumbers = validateNumbers(firstNumber, secondNumber);
}while(validationUserNumbers);

for(let i = firstNumber; i <= secondNumber; i++){
    if (isPrime(i)){
        response += i + ', ';
    }
}

response = response.slice(0, -2);
console.log(response);

function validateNumbers(firstNumber, secondNumber){
return validationUserNumbers = 
!firstNumber || !secondNumber 
|| isNaN(+firstNumber) || isNaN(+secondNumber) 
|| !Number.isInteger(+firstNumber) || !Number.isInteger(+secondNumber);
}

function isPrime(number){
    {
        for(let i = 2, s = Math.sqrt(number); i <= s; i++)
            if(number % i === 0) return false; 
        return number > 1;
    }
}

