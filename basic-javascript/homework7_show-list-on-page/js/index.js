"use strict";

const array = [
 "Kharkiv",
 "Kiev",
 ["Borispol", "Irpin", ['Zarechna', 'Lenina', ['7', '42']]],
 "Odessa",
 "Lviv",
 "Dnieper",
];

let startTime = 2;

showArrayAsList(array);

function showArrayAsList(arr, root = document.body) {
 const fragment = document.createDocumentFragment();
 let array = arr.slice(0,arr.length);
 const ul = buildUl(array);
 fragment.append(ul);
 root.append(fragment);
 doSomeAction(ul);
}

function buildUl(array){
 let newLi = document.createElement("li");
 let ul = document.createElement("ul");
 let li = document.createElement("li");
 if (isHasObject(array)) {
  array = array.map((item) => item);
 }
 array.forEach((item) => {
  if (typeof item === "object") {
   const newUl = buildUl(item);
   ul.appendChild(newUl);
  }
  newLi = li.cloneNode(false);
  if (!(typeof item === "object")) {
   newLi.textContent = item;
   ul.appendChild(newLi);
  }
 });
 return ul;
}

function isHasObject(array){
 let isObject = false;
 array.forEach((item) => {
  if (typeof item === "object" && Object.keys(item).length > 1) {
   isObject = true;
  }
 });
 return isObject;
}

function doSomeAction(node){
 const timerElement = document.getElementById("timer");
  let timerId = setInterval(() => showTime(timerElement), 1000);
  setTimeout(() => { clearInterval(timerId)}, 3000);
  setTimeout(clearNode, 3000, node);
  setTimeout(clearNode, 3000, timerElement);
}

function showTime(timerElement){

 timerElement.innerHTML = startTime;
 startTime--;
}

function clearNode(node){
 node.innerHTML = '';
}