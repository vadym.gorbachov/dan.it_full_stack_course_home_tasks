"use strict";

const tabsTitles = document.querySelectorAll('.tabs-title');
const tabsItems = document.querySelectorAll('.tabs-item');

for (let i = 1; i < tabsItems.length; i++) {
  tabsItems[i].hidden = true;
}

const tabstitleOnClick = (e) => {
  for (const item of tabsTitles) {
    item.className = item.className.replace('active', '');
  }
  e.target.className = e.target.className.concat(' active');

  for (const item of tabsItems){
    if (item.className.split(' ')[1] === e.target.innerHTML.toLowerCase()){
      item.hidden = false;
    }else{
      item.hidden = true;
    }
    item.className = item.className.replace('active', '');
  }
}

for (const tabItem of tabsTitles) {
tabItem.addEventListener('click', tabstitleOnClick);
}
