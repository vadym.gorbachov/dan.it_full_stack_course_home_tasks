"use strict";

const array = ['hello', 'world', true, 23, null, '23', null, {name: 'Masha', age: '21'}];

function filterBy(array, type){
    const newArray = [];
    array.forEach(item => {
    if (!(typeof item === type) ){
        newArray.push(item);
    }
    })
    return newArray;
}

const arrayWithoutNumber = filterBy(array, 'number');
console.log(arrayWithoutNumber);
const arrayWithoutString = filterBy(array, 'string');
console.log(arrayWithoutString);
const arrayWithoutObject = filterBy(array, 'object');
console.log(arrayWithoutObject);
const arrayWithoutBoolean = filterBy(array, 'boolean');
console.log(arrayWithoutBoolean);
