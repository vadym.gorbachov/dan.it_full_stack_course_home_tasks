## Теоретический вопрос

1. Описать своими словами для чего вообще нужны функции в программировании.
2. Описать своими словами, зачем в функцию передавать аргумент.

## Ответ

1. Функуции в програмировании нужны для разделения кода по уровню абстракции, по функциональности (одна функциональность - одна функция), для декомпозиции задачи и использования готовых решений в других местах кода.
2. Аргументы в функцию передаются, если у них не глобальная область видимости и они нужны для выполнения самой функции. Аргументов может быть много, один или отсутствовать. Возвращаемый результат может быть один или отсутствовать.