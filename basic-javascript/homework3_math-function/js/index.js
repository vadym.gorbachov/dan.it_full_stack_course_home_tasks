"use strict";

const WELCOME_FIRST_NUMBER = 'This program calculate simple ariphmetic operations, like `+`, `-`, `*`, `/`. Please enter first number"';
const WELCOME_SECOND_NUMBER = 'This program calculate simple ariphmetic operations, like `+`, `-`, `*`, `/`. Please enter second number"';
const WELCOME_OPERATOR = 'This program calculate simple ariphmetic operations, like `+`, `-`, `*`, `/`. Please enter operator';
let numberOne = '';
let numberTwo = '';
let operator = '';

do{
    const newNumberOne = prompt(WELCOME_FIRST_NUMBER, numberOne);
    numberOne = isNaNull(numberOne, newNumberOne);
    const newNumberTwo = prompt(WELCOME_SECOND_NUMBER, numberTwo);
    numberTwo = isNaNull(numberTwo, newNumberTwo);
    const newOperator = prompt(WELCOME_OPERATOR, operator);
    operator = isNaNull(operator, newOperator);
    if (isExpresion(numberOne, numberTwo, operator)){
        console.log(calculate(numberOne, numberTwo, operator))
        break;
    }
}while(true);

function calculate(numberOne, numberTwo, operator){
    let result;
    switch (operator){
        case '+':{
            result = Number.parseFloat(numberOne) + Number.parseFloat(numberTwo);
            break;
        }
        case '-':{
            result = Number.parseFloat(numberOne) - Number.parseFloat(numberTwo);
            break;
        }
        case '*':{
            result = Number.parseFloat(numberOne) * Number.parseFloat(numberTwo);
            break;
        }
        case '/':{
            result = Number.parseFloat(numberOne) / Number.parseFloat(numberTwo);
            break;
        }
    }
    return result;
}

function isExpresion(numberOne, numberTwo, operator){
    if (isNumber(numberOne) && isNumber(numberTwo) && isOperator(operator)){
        return true;
    }else {
        return false;
    }
}

function isNumber(number){
    if (!!number && !isNaN(number)){
        return true;
    }else{
        return false;
    }
}

function isOperator(operator){
    if (operator === '+' 
        || operator === '-' 
        || operator === '*' 
        || operator === '/'){
        return true;
    }else{
        return false;
    }
}

function isNaNull(value, newValue){
    if (newValue === null){
        return value;
    }
    return newValue;
}







