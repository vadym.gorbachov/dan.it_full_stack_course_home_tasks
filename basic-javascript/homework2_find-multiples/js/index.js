"use strict";

const WELCOME_MESSAGE = 'Calculation numbers ich can divade by 5 without remainder of the division. Please write integer number';
const ATENTION_MESSAGE = 'Please write the correct number';
const ODD_MESSAGE = 'Please enter the odd number';
const SORRY_MESSAGE = 'Sorry, no numbers';

let validationUserNumber = false;
let isOdd = false;
let isEmpty = false;
let userNumber = '';
let response = '';

do{
    validationUserNumber = !userNumber || isNaN(+userNumber);
    if  (validationUserNumber){
        userNumber = prompt(WELCOME_MESSAGE);
    }else{
        isOdd = !(userNumber % 2 === 0)
        if (isOdd){
            userNumber = prompt(ODD_MESSAGE);
        }else{
            userNumber = parseFloat(userNumber);
            if(Number.isInteger(userNumber)){
                break;
            }
        }
    } 
}while(validationUserNumber || isOdd);

for (let i = 5; i <= userNumber; i++){ 
    if ((i % 5) === 0){
            response += i + ', ';
    }
}

if (response) {
    response = response.slice(0, -2);
    console.log(response);
}else{
    console.log(SORRY_MESSAGE);
}


