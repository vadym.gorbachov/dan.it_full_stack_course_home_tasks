"use strict";

const WELCOME_MESSAGE = 'Calculation numbers ich can divade by 5 without remainder of the division. Please write integer number';
const ATENTION_MESSAGE = 'Please write the correct number';
const SORRY_MESSAGE = 'Sorry, no numbers';

let userNumber = prompt(WELCOME_MESSAGE);

do{
    if  (isInvalid(userNumber)){
        userNumber = prompt(ATENTION_MESSAGE);
    }else{ let numbers = searchNumbers(userNumber);
            printCsvResult(numbers);
            break;
        }     
} while(isInvalid(userNumber));

function searchNumbers(n){
    let numbersCsv = '';
    for (let i = 0; i <= n; i += 5){
        numbersCsv += i + ', ';
    }
    return numbersCsv;
}

function printCsvResult(numbersCsv){
    if (numbersCsv) {
        numbersCsv = numbersCsv.slice(0, -2);
        console.log(numbersCsv);
    }else{
        console.log(SORRY_MESSAGE);
    }
}

function isInvalid(number){
    if (
        !number 
        || isNaN(+number)
        || !Number.isInteger(+number)) {
        return true;
    }
        return false;
}




