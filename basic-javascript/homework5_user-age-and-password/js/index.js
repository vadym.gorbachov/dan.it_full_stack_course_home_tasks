"use strict";

const WELCOME_FIRSTNAME_MESSAGE = 'Please enter your firstname';
const WELCOME_LASTNAME_MESSAGE = 'Please enter your lastname';
const WELCOME_BIRTHDAY_MESSAGE = 'Please enter your birthday in format dd.mm.yyyy';
const FIRSNAME = 'FIRSTNAME';
const LASTNAME = 'LASTNAME';
const FIRSTNAME_LASTNAME_REGEX = '^[a-zA-Z][a-zA-Z0-9-]+$';
const BIRTHDAY_REG_EX = `^(((0[1-9]|[12]\\d|3[01])\\.(0[13578]|1[02])\\.(((1[6-9])|[2-9]\\d)\\d{2}))|((0[1-9]|[12]\\d|30)\\.(0[13456789]|1[012])\\.(((1[6-9])|[2-9]\\d)\\d{2}))|((0[1-9]|1\\d|2[0-8])\\.02\\.(((1[6-9])|[2-9]\\d)\\d{2}))|(29\\.02\\.((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$`
const DATE_RATION = 1000 * 3600 * 24 *364;
class User{
    _firstName
    _lastName
    _birthday

setFirstName = function(firstName){
    this._firstName = firstName;
}

setLastName = function(lastName){
    this._lastName = lastName;
}

setBirthday = function (birthday){
        this._birthday = birthday;
}

getFirstName = function(){
    return this._firstName;
}

getLastName = function(){
    return this._lasttName;
}

getLogin = function(){
    return this._firstName.toLowerCase()[0].concat(this._lastName.toLowerCase());
}

getAge = function (){
    const d = this._birthday.split('.');
    const birthdayData = new Date(d[2], d[0], d[1])
    let age =  (new Date() - birthdayData.getTime()) / DATE_RATION;
    return age.toFixed(1);
}
}

function createNewUser(){
    const firstName = isValid(FIRSNAME);
    const lastName = isValid(LASTNAME);
    const birthday = isBirthday();
    const newUser = new User();
    newUser.setFirstName(firstName);
    newUser.setLastName(lastName);
    newUser.setBirthday(birthday);
    return newUser;
}

function isValid(type){
    let message = '';
    let response = null;
    switch (type){
        case 'FIRSTNAME':{
            message = WELCOME_FIRSTNAME_MESSAGE;
            break;
        }
        case  'LASTNAME':{
            message = WELCOME_LASTNAME_MESSAGE;
            break;
        }
    }
    do{
        response = prompt(message);
       if (
           response !== null
               ? !!response.match(FIRSTNAME_LASTNAME_REGEX)
               :''){
           break;
        }else {
       }
    }while (true)
    return response;
}

function isBirthday(birthday) {
    let response = null;
    do {
        response = prompt(WELCOME_BIRTHDAY_MESSAGE);
        if (!!response.match(BIRTHDAY_REG_EX)) {
            break;
        }
    }while (true) ;
    return response;
}
const user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());


