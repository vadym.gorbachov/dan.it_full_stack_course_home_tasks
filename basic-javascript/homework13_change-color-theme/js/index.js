"use strict";

const image =  document.querySelector('.image-of-game');
const stopButton = document.querySelector('.control-menu-button-stop');
const continueButton = document.querySelector('.control-menu-button-continue');
const controlItems = document.querySelectorAll('.control-menu-item');
const themeSwitcher = document.querySelector('.theme-switcher');
const body = document.querySelector('body');
const themesList = [
  {background: '#000', fontColor: '#e1e5ae', objectsColor: '#5e8b44'},
  {background: '#090d61', fontColor: '#c1c4f3', objectsColor: '#555abc'},
]
let slidesCount = 1;
let slideShowStop = false;

stopButton.onclick = () => {
  slideShowStop = true;
}

continueButton.onclick = () => {
  slideShowStop = false;
}

themeSwitcher.onclick = () => {
  switchTheme();
}

const imagesPathList = [];
imagesPathList.push(image.cloneNode().src = './img/slide-show/1.jpg');
imagesPathList.push(image.cloneNode().src = './img/slide-show/2.jpg');
imagesPathList.push(image.cloneNode().src = './img/slide-show/3.jpg');
imagesPathList.push(image.cloneNode().src = './img/slide-show/4.jpg');
const timer = setInterval(changeSlides, 1000);

setTheme();

function changeSlides(){
if (slideShowStop){
  return;
}
image.src = imagesPathList[slidesCount];
slidesCount = (slidesCount === imagesPathList.length - 1)?0:++slidesCount;
}

function setTheme(){
  let themesCount = +loadTheme('count').toString().replace(/[^0-9]/, '');
  console.log('startTheme: ' + themesCount)
  for (let i = 0; i < controlItems.length; i++){
    setStylesForItem(controlItems[i], themesList[themesCount]);
    body.style.background = themesList[themesCount].background;
  }
}

function switchTheme(){
  let themesCount = +loadTheme('count').toString().replace(/[^0-9]/, '');
  themesCount = (themesCount === themesList.length - 1)?0:++themesCount;
for (let i = 0; i < controlItems.length; i++){
  setStylesForItem(controlItems[i], themesList[themesCount]);
  body.style.background = themesList[themesCount].background;
}
  saveTheme('count', themesCount);
}

function setStylesForItem(item, styles){
  item.style.color = styles.fontColor;
  item.style.background = styles.objectsColor;
}

function saveTheme(key, theme){
  try{
    localStorage.setItem(key, JSON.stringify(theme));
  }catch (e){
    console.log(e);
    return false;
  }
  return true;
}

function loadTheme(key){
  const theme = localStorage.getItem(key);
  if(theme === null){
    return 0;
  }
  return theme;
}