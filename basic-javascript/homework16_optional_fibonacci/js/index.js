"use strict";

const WELCOME_FIRST_NUMBER = 'Enter first number for Fibonacci subsequence';
const WELCOME_SECOND_NUMBER = 'Enter second number for Fibonacci subsequence';
const WELCOME_SUBSEQUENCE_NUMBER = 'Enter finite number for Fibonacci subsequence';
const RE_WELCOME_FIRST_NUMBER = 'Reenter first number for Fibonacci subsequence';
const RE_WELCOME_SECOND_NUMBER = 'Reenter second number for Fibonacci subsequence';
const RE_WELCOME_SUBSEQUENCE_NUMBER = 'Reenter finite number for Fibonacci subsequence';

const resultLabel = document.querySelector('.result-calculation');

let f0 = prompt(WELCOME_FIRST_NUMBER);
let f1 = prompt(WELCOME_SECOND_NUMBER);
let n = prompt(WELCOME_SUBSEQUENCE_NUMBER);
if (isInvalid(f0, f1, n)){
    do{
        f0 = prompt(RE_WELCOME_FIRST_NUMBER);
        f1 = prompt(RE_WELCOME_SECOND_NUMBER);
        n = prompt(RE_WELCOME_SUBSEQUENCE_NUMBER);
    }while (isInvalid(f0, f1, n));
}

function isInvalid(f0, f1, n){
    if (
        !!f0 && !isNaN(f0)
        && !!f1 && !isNaN(f1)
        && !!n && !isNaN(n)
        && n >= 1
    ){
        return false;
    }else{
        return true;
    }
}

resultLabel.style.visibility = 'visible';
resultLabel.innerHTML = 'fibonacci number of subsequence: ' + calculateFibonacci(f0, f1, n);

function calculateFibonacci(f0, f1, n){
    let result = f1;
    let count = 0;
do{
    count++
result = f0 = f1;
f1 = +f0 + +f1;
f0 = f1;
}while (count <= n);
return result;
}
