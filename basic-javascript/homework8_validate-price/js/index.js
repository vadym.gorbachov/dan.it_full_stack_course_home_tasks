"use strict";

const ERROR_MESSAGE = 'Please enter correct price';

let priceField = document.querySelector('.curent-prise-field');
const priceForm = document.querySelector('.price-form');
const curentPriceContainer = document.createElement('div');
const closeButton = document.createElement('button');
closeButton.innerHTML = '&#10006;';
const currentPriceMessage = document.createElement('span');
curentPriceContainer.appendChild(currentPriceMessage);
curentPriceContainer.appendChild(closeButton);
const errorMessage = document.createElement('span');
errorMessage.style.display = "block";
errorMessage.className = 'error-message';
errorMessage.innerHTML = ERROR_MESSAGE;
errorMessage.style.color = 'red';

const onInputFocus = (e) => {
  e.target.style.outline = "3px solid green";
}

const onInputBlur = (e) => {
  if (isNaN(priceField.value)
  || priceField.value === '') {
  processIncorrectString(e);
    return;
  } else {
    if (Number.parseInt(priceField.value) < 0) {
      console.log('negative')
      processNegativeValue(e);
  } else {
      processPositiveValue(e);
  }
}
}

function processIncorrectString(e) {
  e.target.style.outline = "none";
  deletePriceMessage();
  deleteErrorMessage();
}

function processNegativeValue(e){
  deletePriceMessage();
  e.target.style.outline = "3px solid red";
  priceForm.append(errorMessage);
  priceField.classList.remove('color-text');
}

function processPositiveValue(e){
  e.target.style.outline = "none";
  currentPriceMessage.innerHTML = `Текущая цена: ${priceField.value}`;
  curentPriceContainer.className = 'curent-price-holder';
  priceForm.prepend(curentPriceContainer);
  priceField.classList.add('color-text');
  deleteErrorMessage();
}

function deleteErrorMessage(){
  if (document.querySelector('.error-message')) {
    errorMessage.parentNode.removeChild(errorMessage);
  }
}

function deletePriceMessage(){
  if (document.querySelector('.curent-price-holder')) {
    curentPriceContainer.parentNode.removeChild(curentPriceContainer);
  }
}

const onCloseButtonClick = (e) => {
  curentPriceContainer.parentNode.removeChild(curentPriceContainer);
  priceField.value = 0;
}

const onSubmit = (e) => {
  e.preventDefault();
}

priceForm.addEventListener('submit', onSubmit);
priceField.addEventListener("focus", onInputFocus);
priceField.addEventListener("blur", onInputBlur);
closeButton.addEventListener('click', onCloseButtonClick);