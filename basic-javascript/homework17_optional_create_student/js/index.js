"use strict";

const WELCOME_FIRSTNAME_MESSAGE = 'Please enter your firstname';
const WELCOME_LASTNAME_MESSAGE = 'Please enter your lastname';
const FIRSTNAME = 'FIRSTNAME';
const LASTNAME = 'LASTNAME';
const WELCOME_SUBJECT_NAME = 'Please enter the subject';
const WELCOME_SUBJECT_GRADE = "Please enter the subject grade";
const FIRSTNAME_LASTNAME_REGEX = '^[a-zA-Z][a-zA-Z0-9-]+$|^[а-яА-Я][а-яА-Я0-9-]+$';
const PASSING_SCORE = 7;
const PASSING_SCORE_TRUE = 'Студенту назначена стипендия';
const PASSING_SCORE_FALSE = 'Студенту не назначена стипендия';
const PASS_COURSE = 'Студент переведен на следующий курс';
const DONT_PASS_COURSE = 'Студент проходит курс повторно';

class Student{
    _firstName
    _lastName
    _table = [];

    constructor(firstName, lastName) {
        this._firstName = firstName;
        this._lastName = lastName;
    }

    setTable = function (table){
        this._table = table;
    }

    isPassCourse = function (){
        let countBadMarks  = 0;
        for (const subject of this._table) {
            if (subject.subjectGrade < PASSING_SCORE){
                countBadMarks++;
            }
        }
        if (countBadMarks < 4){
            return PASS_COURSE;
        }else {
            return DONT_PASS_COURSE;
        }
    }

    calculateScholarship = function (){
        let sum = 0;
        for (const subject of this._table) {
            sum += +subject.subjectGrade;
        }

        if ((sum/this._table.length) >= PASSING_SCORE){
            return PASSING_SCORE_TRUE
        }else{
            return PASSING_SCORE_FALSE;
        }
    }
}

const student = new Student(isValid(FIRSTNAME), isValid(LASTNAME));
student.setTable(getTableDataFromUser());
console.log(student.isPassCourse());
console.log(student.calculateScholarship());

function isValid(type){
    let message = '';
    let response = null;
    switch (type){
        case 'FIRSTNAME':{
            message = WELCOME_FIRSTNAME_MESSAGE;
            break;
        }
        case  'LASTNAME':{
            message = WELCOME_LASTNAME_MESSAGE;
            break;
        }
    }
    do{
        response = prompt(message);
        if (
            response !== null
                ? !! ''
                :response.match(FIRSTNAME_LASTNAME_REGEX)){
            continue;
        }else {
            return response;
        }
    }while (true)
}

function getTableDataFromUser(){
    let servey = true;
    const table = []
    do{
        const subjectName = prompt(WELCOME_SUBJECT_NAME);
        const subjectGrade = prompt(WELCOME_SUBJECT_GRADE);
        if (
            subjectName == null
            || subjectGrade == null
        ){
            servey = false;
        }else {
            table.push({subjectName: subjectName, subjectGrade: subjectGrade});
        }
    }while (servey);
    return table;
}
