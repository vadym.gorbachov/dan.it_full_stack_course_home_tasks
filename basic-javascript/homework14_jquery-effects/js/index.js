"use strict"


const goUp = document.querySelector(".back-to-start");

const hideTopRated = document.querySelector('.show-top-rated');

hideTopRated.addEventListener('click',  (e) =>{
  e.preventDefault();
});

$(".link-to-posts").click(function() {
  $('html, body').animate({
    scrollTop: $(".posts").offset().top
  }, 1000);
});

$(".link-to-rates").click(function() {
  $('html, body').animate({
    scrollTop: $(".rated").offset().top
  }, 2000);
});

$(".link-to-news").click(function() {
  $('html, body').animate({
    scrollTop: $(".popular-news").offset().top
  }, 2000);
});

$(window).scroll(function ()
{
  const content_height = height();
  const content_scroll_pos = $(window).scrollTop();
  if(content_height - content_scroll_pos < 0) {
    goUp.style.display = 'block';
  }else {
    goUp.style.display = 'none';
  }
});

function height() {
  return (window.innerHeight) ?
    window.innerHeight :
    document.documentElement.clientHeight || document.body.clientHeight || 0;
}

  $(".back-to-start").click(function() {
    $('html, body').animate({
      scrollTop: $(".page-header").offset().top
    }, 1500);
  });

$(".show-top-rated").click(function() {
  $( ".rated" ).toggle( "slow", function() {
  });
  $( ".image-sread" ).toggle( "slow", function() {
  });
});