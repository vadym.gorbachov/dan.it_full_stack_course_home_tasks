"use strict";

const UNMATCH_PASSWORD_MESSAGE = `Нужно ввести одинаковые значения`;
const UNVALID_PASSWORD_MESSAGE = 'The password does not meet the requirements';
const WELCOME_MESSAGE = `You are welcome`;
const PASSWORD_EXPRESSION_LIGHT = '^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$';
const PASSWORD_PROMPT_LIGHT = '* Minimum eight characters, at least one letter and one number';

const root = document.body;
const firstPasswordIcon = document.getElementById('first-password-icon');
const secondPasswordIcon = document.getElementById('second-password-icon');
const submitButton = document.querySelector('.form-submit');
const firstInputField = document.querySelector('label.input-wrapper:nth-child(1) > input:nth-child(1)');
const passwordPrompt = document.createElement('span');
passwordPrompt.innerHTML = PASSWORD_PROMPT_LIGHT;
root.append(passwordPrompt);
const secondInputField = document.querySelector('label.input-wrapper:nth-child(2) > input:nth-child(1)');
const errorPasswordMessage = document.createElement('span');
errorPasswordMessage.style.display = 'block';
errorPasswordMessage.className = 'error-message';
errorPasswordMessage.style.color = "red";

function showPasswordField(field, icon){
  if (field.type === 'password'){
    field.setAttribute('type', 'text');
    icon.className = icon.className.replace('fa-eye', 'fa-eye-slash');
  }else{
    field.setAttribute('type', 'password');
    icon.className = icon.className.replace('fa-eye-slash', 'fa-eye');
  }
}

function comparePasswordsLight(e){
  e.preventDefault();
  if (firstInputField.value !== secondInputField.value){
    showErrorMessage(UNMATCH_PASSWORD_MESSAGE);
  }else {
    if (!firstInputField.value.match(PASSWORD_EXPRESSION_LIGHT)){
      showErrorMessage(UNVALID_PASSWORD_MESSAGE);
    }else{
      errorPasswordMessage.hidden = true;
      alert(WELCOME_MESSAGE);
    }
  }
}

function showErrorMessage(message){
  if (document.getElementsByClassName('error-message').length > 0){
    errorPasswordMessage.innerHTML = message;
  }else {
    errorPasswordMessage.innerHTML = message;
    secondInputField.after(errorPasswordMessage);
  }
}

firstPasswordIcon.addEventListener('click', () => {
  showPasswordField(firstInputField, firstPasswordIcon);
});

secondPasswordIcon.addEventListener('click', () => {
  showPasswordField(secondInputField, secondPasswordIcon);
});

submitButton.addEventListener('click', comparePasswordsLight);
