"use strict";

const image =  document.querySelector('.image-of-game');
const stopButton = document.querySelector('.control-menu-button-stop');
const continueButton = document.querySelector('.control-menu-button-continue');
let count = 1;
let slideShowStop = false;

stopButton.onclick = () => {
  slideShowStop = true;
}

continueButton.onclick = () => {
  slideShowStop = false;
}

const imagesPathList = [];
imagesPathList.push(image.cloneNode().src = './img/slide-show/1.jpg');
imagesPathList.push(image.cloneNode().src = './img/slide-show/2.jpg');
imagesPathList.push(image.cloneNode().src = './img/slide-show/3.jpg');
imagesPathList.push(image.cloneNode().src = './img/slide-show/4.jpg');

const timer = setInterval(changeSlides, 1000);

function changeSlides(){
if (slideShowStop){
  return;
}
image.src = imagesPathList[count];
count = (count === imagesPathList.length - 1)?0:++count;
}
