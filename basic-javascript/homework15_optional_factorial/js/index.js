"use strict";

const calculateFactorialButton = document.querySelector('.calculate-button');
const input = document.querySelector('.input-field');
const resultLabel = document.querySelector('.result-calculation');

calculateFactorialButton.addEventListener('click', calculate);

function calculate(){
    resultLabel.innerHTML = 'fractal: ' + calculateFactorial(input.value);
}

function calculateFactorial (number){
 return  (y =>
            y(
                factorial =>
                    n =>
                        n < 2 ? 1 : n * factorial(n-1)
            )(number)
    )(le =>
        (f =>
                f(f)
        )(f =>
            le(x => (f(f))(x))
        )
    );
}
